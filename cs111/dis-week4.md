Midterm Review
=============
Kernel Mode: 
	Unrestricted access to the hardware
	Can execute any priviledged instructions

1. Name several ways in which the processor can transition from user mode to kernel mode.
	- System Calls
	- External Interrupts
	- Execute priviledged instructions (INT, HLT)
	- Access to privileged register (CR3 - address of the page table)
	- System fault (access to invalid memory addresses, divide by 0)

